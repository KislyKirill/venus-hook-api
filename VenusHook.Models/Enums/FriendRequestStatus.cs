﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VenusHook.Models.Enums
{
    public enum FriendRequestStatus
    {
        Accept,
        Reject,
        Waiting
    }
}

﻿namespace VenusHook.Models.Settings
{
    public class ApplicationSettings
    {
        public string JwtSecret { get; set; }
        public string ClientUrl { get; set; }
    }
}

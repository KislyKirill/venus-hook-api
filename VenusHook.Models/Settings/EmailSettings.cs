﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VenusHook.Models.Settings
{
    public class EmailSettings
    {
        public string MailService { get; set; }
        public int MailPort { get; set; }
        public string SenderName { get; set; }
        public string Sender { get; set; }
        public string Password { get; set; }
    }
}

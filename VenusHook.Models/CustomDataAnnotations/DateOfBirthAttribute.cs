﻿using System;
using System.ComponentModel.DataAnnotations;
using VenusHook.Models.Helpers;

namespace VenusHook.Models.CustomDataAnnotations
{
    public class DateOfBirthAttribute : ValidationAttribute
    {
        public DateOfBirthAttribute() { }

        public override bool IsValid(object value)
        {
            var dateTime = (DateTime)value;
            var ageMin = DateTime.Now.AddYears(-DataConstants.MinUserAge);
            var ageMax = DateTime.Now.AddYears(-DataConstants.MaxUserAge);

            if (dateTime <= ageMin && dateTime >= ageMax)
            {
                return true;
            }

            return false;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using VenusHook.Models.Entities.Type;

namespace VenusHook.Models.Entities
{
    public class UserTypeLanguages
    {
        public int Id { get; set; }
        public int UserTypeId { get; set; }
        public int LanguageId { get; set; }

        public virtual Language Language { get; set; }
    }
}

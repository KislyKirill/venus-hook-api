﻿using System.Collections.Generic;
using VenusHook.Models.Entities.Type;
using VenusHook.Models.Helpers;

namespace VenusHook.Models.Entities
{
    public class UserType : IEntity
    {
        public int Id { get; set; }

        public int Height { get; set; }
        public int Weight { get; set; }

        public string Interests { get; set; }
        public string TargetRelationship { get; set; }
        public int? EducationId { get; set; }
        public int? NationalityId { get; set; }
        public int? FinancialSituationId { get; set; }
        public int? FamilyStatusId { get; set; }
        public int? ZodiacSignId { get; set; }

        public virtual FamilyStatus FamilyStatus { get; set; }
        public virtual ZodiacSign ZodiacSign { get; set; }
        public virtual Education Education { get; set; }
        public virtual Nationality Nationality { get; set; }
        public virtual FinancialSituation FinancialSituation { get; set; }

        public virtual ICollection<UserTypeBadHabits> UserTypeBadHabits { get; set; }
        public virtual ICollection<UserTypeLanguages> UserTypeLanguages { get; set; }
    }
}
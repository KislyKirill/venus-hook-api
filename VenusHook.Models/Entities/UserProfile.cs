﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace VenusHook.Models.Entities
{
    public class UserProfile
    {
        [Key]
        [ForeignKey("User")]
        public string Id { get; set; }
        public bool? IsPrivate { get; set; } = false;
        public DateTime? LastActive { get; set; }

        public int? StatisticsId { get; set; }
        public int? UserTypeId { get; set; }
        public int? AvatarId { get; set; }
        public string AvatarUrl { get; set; }

        public virtual Statistics Statistics { get; set; }
        public virtual UserType UserType { get; set; }
        public virtual User User { get; set; }

        public virtual ICollection<Photo> Photos { get; set; }
        public virtual ICollection<FriendshipList> FriendshipsList { get; set; }
        public virtual ICollection<BlackList> BlackList { get; set; }

        public virtual ICollection<FriendRequest> FriendRequestSent { get; set; } = new List<FriendRequest>();
        public virtual ICollection<FriendRequest> FriendRequestReceived { get; set; } = new List<FriendRequest>();

        public virtual ICollection<Message> MessagesSent { get; set; } = new List<Message>();
        public virtual ICollection<Message> MessagesReceived { get; set; } = new List<Message>();
    }
}

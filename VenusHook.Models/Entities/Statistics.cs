﻿using VenusHook.Models.Helpers;

namespace VenusHook.Models.Entities
{
    public class Statistics : IEntity
    {
        public int Id { get; set; }
        public int CountView { get; set; }
        public double ReplyRate { get; set; }
    }
}

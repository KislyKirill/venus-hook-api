﻿using VenusHook.Models.Helpers;

namespace VenusHook.Models.Entities
{
    public class FriendshipList : IEntity
    {
        public int Id { get; set; }

        public string UserProfileId { get; set; }
        public string FriendId { get; set; }

        public virtual UserProfile UserProfile { get; set; }
        public virtual UserProfile Friend { get; set; }
    }
}

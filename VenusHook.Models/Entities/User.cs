﻿using Microsoft.AspNetCore.Identity;
using System;
using System.ComponentModel.DataAnnotations;
using VenusHook.Models.CustomDataAnnotations;
using VenusHook.Models.Entities.Type;
using VenusHook.Models.Helpers;

namespace VenusHook.Models.Entities
{
    public class User : IdentityUser
    {
        [Required]
        public override string UserName { get; set; }

        [MinLength(DataConstants.NameMinLength), MaxLength(DataConstants.NameMaxLength)]
        public string FirstName { get; set; }

        [MinLength(DataConstants.NameMinLength), MaxLength(DataConstants.NameMaxLength)]
        public string LastName { get; set; }

        [DateOfBirth]
        public DateTime DateOfBirth { get; set; }
        public int GenderId { get; set; }
        public bool IsFirstLogin { get; set; }

        public virtual Gender Gender { get; set; }
        public virtual UserProfile UserProfile { get; set; }
    }
}
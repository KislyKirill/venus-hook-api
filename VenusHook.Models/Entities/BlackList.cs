﻿using VenusHook.Models.Helpers;

namespace VenusHook.Models.Entities
{
    public class BlackList : IEntity
    {
        public int Id { get; set; }

        public string UserProfileId { get; set; }
        public string BlockUserId { get; set; }

        public virtual UserProfile UserProfile { get; set; }
        public virtual UserProfile BlockUser { get; set; }
    }
}

﻿using System.ComponentModel.DataAnnotations;
using VenusHook.Models.Entities.Type;
using VenusHook.Models.Enums;
using VenusHook.Models.Helpers;

namespace VenusHook.Models.Entities
{
    public class FriendRequest : IEntity
    {
        public int Id { get; set; }

        public string SenderId { get; set; }
        public string ReceiverId { get; set; }

        public virtual UserProfile Sender { get; set; }
        public virtual UserProfile Receiver { get; set; }

        public FriendRequestStatus FriendRequestStatus { get; set; }
    }
}

﻿using System;
using System.ComponentModel.DataAnnotations;
using VenusHook.Models.Helpers;

namespace VenusHook.Models.Entities
{
    public class Message : IEntity
    {
        public int Id { get; set; }

        [Required]
        public string SenderId { get; set; }

        [Required]
        public string ReceiverId { get; set; }

        [Required]
        [MaxLength(DataConstants.MaxMessageLength)]
        public string MessageText { get; set; }

        [Required]
        public DateTime DateSent { get; set; }

        public bool IsSeen { get; set; }

        [Required]
        public virtual UserProfile Sender { get; set; }

        [Required]
        public virtual UserProfile Receiver { get; set; }
    }
}

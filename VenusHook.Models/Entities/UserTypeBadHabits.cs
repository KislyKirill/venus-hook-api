﻿using System;
using System.Collections.Generic;
using System.Text;
using VenusHook.Models.Entities.Type;

namespace VenusHook.Models.Entities
{
    public class UserTypeBadHabits
    {
        public int Id { get; set; }
        public int UserTypeId { get; set; }
        public int BadHabitId { get; set; }

        public virtual BadHabit BadHabit { get; set; }
    }
}

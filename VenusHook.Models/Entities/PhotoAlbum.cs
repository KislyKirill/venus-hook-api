﻿using System.Collections.Generic;

namespace VenusHook.Models.Entities
{
    public class PhotoAlbum
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Path { get; set; }
        public string UserProfileId { get; set; }
        public virtual ICollection<Photo> Photos { get; set; }
    }
}

﻿using VenusHook.Models.Helpers;

namespace VenusHook.Models.Entities
{
    public class Photo : IEntity
    {
        public int Id { get; set; }
        public string Url { get; set; }
        public int? PhotoAlbumId { get; set; }
        public string UserProfileId { get; set; }

        public virtual PhotoAlbum PhotoAlbum { get; set; }
        public virtual UserProfile UserProfile { get; set; }
    }
}
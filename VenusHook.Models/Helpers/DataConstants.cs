﻿namespace VenusHook.Models.Helpers
{
    public class DataConstants
    {
        public const int MaxPhotoLength = 5 * 1024 * 1024;
        public const int MaxMessageLength = 2000;
        public const int MaxUserAge = 130;
        public const int MinUserAge = 16;
        public const int NameMinLength = 2;
        public const int NameMaxLength = 50;
    }
}
﻿namespace VenusHook.Models.Helpers
{
    public class BaseType : IEntity
    {
        public int Id { get; set; }
        public string Value { get; set; }
    }
}

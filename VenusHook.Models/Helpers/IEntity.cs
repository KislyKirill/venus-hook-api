﻿namespace VenusHook.Models.Helpers
{
    public interface IEntity
    {
        int Id { get; set; }
    }
}

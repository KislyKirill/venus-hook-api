﻿using System.Threading.Tasks;
using VenusHook.BLL.Helpers;
using VenusHook.BLL.ViewModels.Settings;
using VenusHook.BLL.ViewModels.User;

namespace VenusHook.BLL.Interfaces
{
    public interface IUserService
    {
        Task<ResultHelper> Register(RegisterViewModel model, string host);
        Task<ResultHelper> Login(LoginViewModel model);
        Task<ResultHelper> ConfirmationEmail(string userId, string code);

        Task<ResultHelper> ChangePassword(ChangePasswordViewModel model);
    }
}
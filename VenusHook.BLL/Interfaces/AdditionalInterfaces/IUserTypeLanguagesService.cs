﻿using System.Threading.Tasks;

namespace VenusHook.BLL.Interfaces.AdditionalInterfaces
{
    public interface IUserTypeLanguagesService
    {
        void DeleteLanguages(int id);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VenusHook.BLL.Interfaces.AdditionalInterfaces
{
    public interface IUserTypeBadHabitsService
    {
        void DeleteBadHabits(int id);
    }
}

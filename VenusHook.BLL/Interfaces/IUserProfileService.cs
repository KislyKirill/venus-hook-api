﻿using System.Threading.Tasks;
using VenusHook.BLL.Helpers;
using VenusHook.BLL.ViewModels.Settings;
using VenusHook.BLL.ViewModels.UserProfile;
using VenusHook.BLL.ViewModels.UserProfile.EditProfile;
using VenusHook.BLL.ViewModels.UserType;
using VenusHook.Models.Entities;

namespace VenusHook.BLL.Interfaces
{
    public interface IUserProfileService
    {
        Task<UserProfile> GetUserProfile(string id);
        Task<ResultHelper> EditMainInformation(EditMainViewModel model);
        Task<ResultHelper> EditMoreInformation(MoreRegisterViewModel userTypeVM, UserProfile userProfile);
        Task<ResultHelper> IsPrivateProfile(PrivateViewModel model);
    }
}
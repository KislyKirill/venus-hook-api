﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VenusHook.BLL.ViewModels.Search;
using VenusHook.Models.Entities;

namespace VenusHook.BLL.Interfaces
{
    public interface ISearchService
    {
        IEnumerable<UserProfile> GetAllUserProfiles(string currentUserId);
        IQueryable<UserProfile> FindUsersByCriteria(SearchByCriteriaViewModel model);
    }
}

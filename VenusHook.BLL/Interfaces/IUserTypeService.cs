﻿using VenusHook.BLL.ViewModels.UserType;

namespace VenusHook.BLL.Interfaces
{
    public interface IUserTypeService
    {
        UserTypeAllVM GetAllAdditionalInfo();
    }
}

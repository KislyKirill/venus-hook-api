﻿using System.Threading.Tasks;

namespace VenusHook.BLL.Interfaces
{
    public interface IEmailSenderService
    {
        Task SendEmailAsync(string email, string subject, string message);
    }
}

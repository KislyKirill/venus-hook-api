﻿using System.Threading.Tasks;
using VenusHook.BLL.ViewModels.UserProfile.UserPhoto;

namespace VenusHook.BLL.Interfaces
{
    public interface IFileService
    {
        Task AddUserPhoto(PhotoViewModel model);
        Task AddPhotoInAlbum(PhotoAlbumViewModel model);
        Task DeletePhoto(int id);
    }
}

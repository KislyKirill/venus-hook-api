﻿using System.Collections.Generic;
using System.Threading.Tasks;
using VenusHook.BLL.ViewModels.Friendships;
using VenusHook.Models.Entities;

namespace VenusHook.BLL.Interfaces
{
    public interface IFriendService
    {
        Task<IEnumerable<FriendshipList>> GetAllFriends(string userProfileId);
        Task<IEnumerable<FriendRequest>> FindAllRequests(string userProfileId);
        Task<FriendRequest> SendRequest(FriendRequestViewModel model);
        Task AcceptedRequest(int id);
        Task RejectedRequest(int id);
        Task RemoveFriend(string userProfileId, string friendId);
    }
}

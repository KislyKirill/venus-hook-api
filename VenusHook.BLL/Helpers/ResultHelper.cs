﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VenusHook.BLL.Helpers
{
    public class ResultHelper
    {
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
        public object Property { get; set; }
    }
}

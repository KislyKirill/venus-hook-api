﻿using Microsoft.AspNetCore.Http;
using System;
using System.Linq;

namespace VenusHook.BLL.Helpers
{
    public class UploadFileHelper
    {
        public string ErrorMessage { get; set; }
        public decimal FileSize { get; set; }

        public string FileUploadValidation(IFormFile file)
        {
            try
            {
                var supportedTypes = new[] { "jpg", "jpeg", "png" };
                //var supportedTypes = new[] { "txt", "doc", "docx", "pdf", "xls", "xlsx" };
                var fileExt = System.IO.Path.GetExtension(file.FileName).Substring(1);
                if (!supportedTypes.Contains(fileExt))
                {
                    ErrorMessage = "File extension is invalid - only upload jpg/jpeg/png file";
                    return ErrorMessage;
                }
                else if (file.Length > (FileSize * 1048576))
                {
                    ErrorMessage = "File size should be up to " + FileSize + " MB";
                    return ErrorMessage;
                }
                else
                {
                    // File is successfully
                    return null;
                }
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message;
                return ErrorMessage;
            }
        }
    }
}

﻿using AutoMapper;
using System.Collections;
using System.Collections.Generic;
using VenusHook.BLL.ViewModels.Friendships;
using VenusHook.BLL.ViewModels.StaticBase;
using VenusHook.BLL.ViewModels.User;
using VenusHook.BLL.ViewModels.UserProfile;
using VenusHook.BLL.ViewModels.UserProfile.EditProfile;
using VenusHook.BLL.ViewModels.UserType;
using VenusHook.Models.Entities;
using VenusHook.Models.Entities.Type;

namespace VenusHook.BLL.Helpers
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            // Authentication
            CreateMap<RegisterViewModel, User>()
                .ForMember(dest => dest.Gender, opt => opt.MapFrom(src => src.Gender));
            CreateMap<LoginViewModel, User>();

            // User
            CreateMap<EditMainViewModel, User>();

            // User Profile
            CreateMap<UserProfileViewModel, UserProfile>();

            // User type
            CreateMap<UserTypeViewModel, UserType>();

            // Search 
            CreateMap<IEnumerable<UserProfileViewModel>, List<UserProfile>>();

            // Friendship
            CreateMap<FriendRequestViewModel, FriendRequest>();
            CreateMap<FriendshipListViewModel, FriendshipList>();
            CreateMap<IEnumerable<FriendRequestViewModel>, List<FriendRequest>>();


            // Base view model
            CreateMap<BaseViewModel, Education>();
            CreateMap<BaseViewModel, Nationality>();
            CreateMap<BaseViewModel, FinancialSituation>();
            CreateMap<BaseViewModel, BadHabit>();
            CreateMap<BaseViewModel, Language>();
            CreateMap<BaseViewModel, FamilyStatus>();
            CreateMap<BaseViewModel, Gender>();
        }
    }
}

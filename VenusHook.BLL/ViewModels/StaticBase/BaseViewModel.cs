﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VenusHook.BLL.ViewModels.StaticBase
{
    public class BaseViewModel
    {
        public int Id { get; set; }
        public string Value { get; set; }
    }
}

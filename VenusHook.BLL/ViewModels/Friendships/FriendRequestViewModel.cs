﻿using System;
using System.Collections.Generic;
using System.Text;
using VenusHook.BLL.ViewModels.StaticBase;
using VenusHook.BLL.ViewModels.UserProfile;
using VenusHook.Models.Enums;

namespace VenusHook.BLL.ViewModels.Friendships
{
    public class FriendRequestViewModel
    {
        public int Id { get; set; }

        public string SenderId { get; set; }
        public string ReceiverId { get; set; }

        public virtual UserProfileViewModel Sender { get; set; }
        public virtual UserProfileViewModel Receiver { get; set; }

        public FriendRequestStatus FriendRequestStatus { get; set; }
    }
}

﻿using VenusHook.BLL.ViewModels.UserProfile;

namespace VenusHook.BLL.ViewModels.Friendships
{
    public class BlackListViewModel
    {
        public int Id { get; set; }

        public string UserProfileId { get; set; }
        public string BlockUserId { get; set; }

        public virtual UserProfileViewModel UserProfile { get; set; }
        public virtual UserProfileViewModel BlockUser { get; set; }
    }
}

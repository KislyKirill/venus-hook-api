﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VenusHook.BLL.ViewModels.UserProfile.EditProfile
{
    public class EditMoreViewModel
    {
        public int Height { get; set; }
        public int Weight { get; set; }

        public string Interests { get; set; }
        public string TargetRelationship { get; set; }
        public int? EducationId { get; set; }
        public int? NationalityId { get; set; }
        public int? FinancialSituationId { get; set; }
        public int? FamilyStatusId { get; set; }
        public int? ZodiacSignId { get; set; }

        public List<int> BadHabits { get; set; }
        public List<int> Languages { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using VenusHook.BLL.ViewModels.StaticBase;
using VenusHook.Models.CustomDataAnnotations;

namespace VenusHook.BLL.ViewModels.UserProfile.EditProfile
{
    public class EditMainViewModel
    {
        public string Id { get; set; }
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int GenderId { get; set; }
        [DateOfBirth]
        public DateTime DateOfBirth { get; set; }

        public virtual BaseViewModel Gender { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using VenusHook.BLL.ViewModels.Friendships;
using VenusHook.BLL.ViewModels.StaticBase;
using VenusHook.BLL.ViewModels.User;
using VenusHook.BLL.ViewModels.UserProfile.UserPhoto;
using VenusHook.BLL.ViewModels.UserType;

namespace VenusHook.BLL.ViewModels.UserProfile
{
    public class UserProfileViewModel
    {
        public string Id { get; set; }
        public bool? IsPrivate { get; set; } = false;
        public DateTime? LastActive { get; set; }

        public int? StatisticsId { get; set; }
        public int? UserTypeId { get; set; }
        public int? AvatarId { get; set; }
        public string AvatarUrl { get; set; }

        public virtual StatisticsViewModel Statistics { get; set; }
        public virtual UserTypeViewModel UserType { get; set; }
        public virtual UserViewModel User { get; set; }

        public virtual ICollection<FriendshipListViewModel> FriendshipsList { get; set; }
        public virtual ICollection<BlackListViewModel> BlackList { get; set; }
        public virtual ICollection<FriendRequestViewModel> FriendRequestSent { get; set; }
        public virtual ICollection<FriendRequestViewModel> FriendRequestReceived { get; set; }

        //public virtual ICollection<PhotoViewModel> Photos { get; set; }
    }
}
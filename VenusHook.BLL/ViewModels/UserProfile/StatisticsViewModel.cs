﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VenusHook.BLL.ViewModels.UserProfile
{
    public class StatisticsViewModel
    {
        public int Id { get; set; }
        public int CountView { get; set; }
        public double ReplyRate { get; set; }
    }
}

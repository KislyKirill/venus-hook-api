﻿using AutoMapper.Configuration.Annotations;
using Microsoft.AspNetCore.Http;

namespace VenusHook.BLL.ViewModels.UserProfile.UserPhoto
{
    public class PhotoViewModel
    {
        public int Id { get; set; }
        public string Url { get; set; }
        public int? PhotoAlbumId { get; set; }
        public string UserProfileId { get; set; }
        public UserProfileViewModel UserProfile { get; set; }
        [Ignore]
        public IFormFile Photo { get; set; }
    }
}

﻿using Microsoft.AspNetCore.Http;

namespace VenusHook.BLL.ViewModels.UserProfile.UserPhoto
{
    public class PhotoAlbumViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Path { get; set; }
        public string UserProfileId { get; set; }
        public IFormFileCollection Photos { get; set; }
    }
}
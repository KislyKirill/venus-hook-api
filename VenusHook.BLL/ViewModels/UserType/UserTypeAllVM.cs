﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VenusHook.BLL.ViewModels.StaticBase;

namespace VenusHook.BLL.ViewModels.UserType
{
    public class UserTypeAllVM
    {
        public IEnumerable<BaseViewModel> Education { get; set; }
        public IEnumerable<BaseViewModel> Nationalities { get; set; }
        public IEnumerable<BaseViewModel> FinancialSituation { get; set; }
        public IEnumerable<BaseViewModel> BadHabits { get; set; }
        public IEnumerable<BaseViewModel> Languages { get; set; }
        public IEnumerable<BaseViewModel> FamilyStatus { get; set; }
        public IEnumerable<BaseViewModel> Genders { get; set; }
    }
}

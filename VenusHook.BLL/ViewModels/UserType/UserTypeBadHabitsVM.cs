﻿using System;
using System.Collections.Generic;
using System.Text;
using VenusHook.BLL.ViewModels.StaticBase;

namespace VenusHook.BLL.ViewModels.UserType
{
    public class UserTypeBadHabitsVM
    {
        public int Id { get; set; }
        public int UserTypeId { get; set; }
        public int BadHabitId { get; set; }

        public virtual BaseViewModel BadHabit { get; set; }
    }
}

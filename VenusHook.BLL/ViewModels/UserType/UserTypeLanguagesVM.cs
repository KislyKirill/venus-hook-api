﻿using System;
using System.Collections.Generic;
using System.Text;
using VenusHook.BLL.ViewModels.StaticBase;

namespace VenusHook.BLL.ViewModels.UserType
{
    public class UserTypeLanguagesVM
    {
        public int Id { get; set; }
        public int UserTypeId { get; set; }
        public int LanguageId { get; set; }

        public virtual BaseViewModel Language { get; set; }
    }
}

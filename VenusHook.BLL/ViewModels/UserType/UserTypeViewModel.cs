﻿using AutoMapper.Configuration.Annotations;
using System;
using System.Collections.Generic;
using System.Text;
using VenusHook.BLL.ViewModels.StaticBase;

namespace VenusHook.BLL.ViewModels.UserType
{
    public class UserTypeViewModel
    {
        public int Id { get; set; }

        public int Height { get; set; }
        public int Weight { get; set; }

        public string Interests { get; set; }
        public string TargetRelationship { get; set; }
        public int? EducationId { get; set; }
        public int? NationalityId { get; set; }
        public int? FinancialSituationId { get; set; }
        public int? FamilyStatusId { get; set; }
        public int? ZodiacSignId { get; set; }

        public virtual BaseViewModel FamilyStatus { get; set; }
        public virtual BaseViewModel ZodiacSign { get; set; }
        public virtual BaseViewModel Education { get; set; }
        public virtual BaseViewModel Nationality { get; set; }
        public virtual BaseViewModel FinancialSituation { get; set; }

        public virtual ICollection<UserTypeBadHabitsVM> UserTypeBadHabits { get; set; }
        public virtual ICollection<UserTypeLanguagesVM> UserTypeLanguages { get; set; }
    }
}

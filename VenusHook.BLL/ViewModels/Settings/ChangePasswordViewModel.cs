﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VenusHook.BLL.ViewModels.Settings
{
    public class ChangePasswordViewModel
    {
        public string Id { get; set; }
        public string NewPassword { get; set; }
        public string OldPassword { get; set; }
    }
}

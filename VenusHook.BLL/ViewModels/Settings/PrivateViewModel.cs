﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VenusHook.BLL.ViewModels.Settings
{
    public class PrivateViewModel
    {
        public bool IsPrivate { get; set; }
        public string UserProfileId { get; set; }
    }
}

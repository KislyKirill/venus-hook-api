﻿using AutoMapper.Configuration.Annotations;
using System;
using System.Collections.Generic;
using System.Text;
using VenusHook.BLL.Helpers;
using VenusHook.BLL.ViewModels.StaticBase;
using VenusHook.BLL.ViewModels.UserProfile;

namespace VenusHook.BLL.ViewModels.User
{
    public class UserViewModel
    {
        public string Email { get; set; }
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime DateOfBirth { get; set; }
        public int Age => CalculateDataHelper.CalculateAge(DateOfBirth);
        public bool IsFirstLogin { get; set; }
        public virtual BaseViewModel Gender { get; set; }
        public virtual UserProfileViewModel UserProfile { get; set; }
    }
}

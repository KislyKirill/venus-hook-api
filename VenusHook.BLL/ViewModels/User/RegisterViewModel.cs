﻿using System;
using System.Collections.Generic;
using System.Text;
using VenusHook.BLL.ViewModels.StaticBase;
using VenusHook.Models.CustomDataAnnotations;

namespace VenusHook.BLL.ViewModels.User
{
    public class RegisterViewModel
    {
        public string UserName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int GenderId { get; set; }
        [DateOfBirth]
        public DateTime DateOfBirth { get; set; }
        public BaseViewModel Gender { get; set; }
    }
}
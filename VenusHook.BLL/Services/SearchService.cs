﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VenusHook.BLL.Interfaces;
using VenusHook.BLL.ViewModels.Search;
using VenusHook.DAL.Interfaces;
using VenusHook.Models.Entities;

namespace VenusHook.BLL.Services
{
    public class SearchService : ISearchService
    {
        private IUnitOfWork Database { get; set; }

        public SearchService(IUnitOfWork uow)
        {
            Database = uow;
        }

        public IQueryable<UserProfile> FindUsersByCriteria(SearchByCriteriaViewModel model)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<UserProfile> GetAllUserProfiles(string currentUserId)
        {
            //var userProfiles = Database.UserProfiles.GetAll();
            var userProfiles = Database.UserProfiles.GetWithInclude(t => t.IsPrivate == false && t.Id != currentUserId, x => x.User, u => u.UserType);

            return userProfiles;
        }
    }
}
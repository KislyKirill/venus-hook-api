﻿using AutoMapper;
using System.Collections.Generic;
using VenusHook.BLL.Interfaces;
using VenusHook.BLL.ViewModels.StaticBase;
using VenusHook.BLL.ViewModels.UserType;
using VenusHook.DAL.Interfaces;

namespace VenusHook.BLL.Services
{
    public class UserTypeService : IUserTypeService
    {
        private IUnitOfWork Database { get; set; }
        private readonly IMapper _mapper;

        public UserTypeService(IUnitOfWork uow, IMapper mapper)
        {
            Database = uow;
            _mapper = mapper;
        }

        public UserTypeAllVM GetAllAdditionalInfo()
        {
            var userTypes = new UserTypeAllVM
            {
                Education = _mapper.Map<List<BaseViewModel>>(Database.Education.GetAll()),
                Nationalities = _mapper.Map<List<BaseViewModel>>(Database.Nationalities.GetAll()),
                FinancialSituation = _mapper.Map<List<BaseViewModel>>(Database.FinancialSituations.GetAll()),
                BadHabits = _mapper.Map<List<BaseViewModel>>(Database.BadHabits.GetAll()),
                Languages = _mapper.Map<List<BaseViewModel>>(Database.Languages.GetAll()),
                FamilyStatus = _mapper.Map<List<BaseViewModel>>(Database.FamilyStatus.GetAll()),
                Genders = _mapper.Map<List<BaseViewModel>>(Database.Gender.GetAll()),
            };

            return userTypes;
        }
    }
}

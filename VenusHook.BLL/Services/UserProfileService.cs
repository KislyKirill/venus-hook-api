﻿using AutoMapper;
using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using VenusHook.BLL.Helpers;
using VenusHook.BLL.Interfaces;
using VenusHook.BLL.Interfaces.AdditionalInterfaces;
using VenusHook.BLL.ViewModels.Settings;
using VenusHook.BLL.ViewModels.UserProfile;
using VenusHook.BLL.ViewModels.UserProfile.EditProfile;
using VenusHook.BLL.ViewModels.UserType;
using VenusHook.DAL.Interfaces;
using VenusHook.Models.Entities;
using VenusHook.Models.Entities.Type;

namespace VenusHook.BLL.Services
{
    public class UserProfileService : IUserProfileService
    {
        private IUnitOfWork Database { get; set; }
        private readonly IEmailSenderService _emailSenderService;
        private readonly IMapper _mapper;
        private readonly IUserTypeLanguagesService _userTypeLanguagesService;
        private readonly IUserTypeBadHabitsService _userTypeBadHabitsService;

        public UserProfileService(IUnitOfWork uow,
            IEmailSenderService emailSenderService,
            IMapper mapper,
            IUserTypeLanguagesService userTypeLanguagesService,
            IUserTypeBadHabitsService userTypeBadHabitsService)
        {
            Database = uow;
            _emailSenderService = emailSenderService;
            _mapper = mapper;
            _userTypeLanguagesService = userTypeLanguagesService;
            _userTypeBadHabitsService = userTypeBadHabitsService;
        }

        public async Task<UserProfile> GetUserProfile(string id)
        {
            if (id == null)
                throw new ValidationException("User profile id not set");

            var userProfile = Database.UserProfiles.GetById(id);

            if (userProfile == null)
                throw new ValidationException("User profile not found");

            return await userProfile;
        }

        public async Task<ResultHelper> EditMoreInformation(MoreRegisterViewModel userTypeVM, UserProfile userProfile)
        {
            var result = new ResultHelper();

            if (userProfile == null)
            {
                result.IsSuccess = false;
                result.Message = "User profile not found";
            }


            if (!string.IsNullOrEmpty(userTypeVM.Interests))
                userProfile.UserType.Interests = userTypeVM.Interests;
            else
                userProfile.UserType.Interests = null;

            if (!string.IsNullOrEmpty(userTypeVM.TargetRelationship))
                userProfile.UserType.TargetRelationship = userTypeVM.TargetRelationship;
            else
                userProfile.UserType.TargetRelationship = null;

            if (!string.IsNullOrEmpty(userTypeVM.EducationId.ToString()))
            {
                if (userTypeVM.EducationId.ToString() == "-1")
                    userProfile.UserType.EducationId = null;
                else
                    userProfile.UserType.EducationId = userTypeVM.EducationId;
            }
            if (!string.IsNullOrEmpty(userTypeVM.FamilyStatusId.ToString()))
            {
                if (userTypeVM.FamilyStatusId.ToString() == "-1")
                    userProfile.UserType.FamilyStatusId = null;
                else
                    userProfile.UserType.FamilyStatusId = userTypeVM.FamilyStatusId;
            }
            if (!string.IsNullOrEmpty(userTypeVM.FinancialSituationId.ToString()))
            {
                if (userTypeVM.FinancialSituationId.ToString() == "-1")
                    userProfile.UserType.FinancialSituationId = null;
                else
                    userProfile.UserType.FinancialSituationId = userTypeVM.FinancialSituationId;
            }
            if (!string.IsNullOrEmpty(userTypeVM.NationalityId.ToString()))
            {
                if (userTypeVM.NationalityId.ToString() == "-1")
                    userProfile.UserType.NationalityId = null;
                else
                    userProfile.UserType.NationalityId = userTypeVM.NationalityId;
            }
            if (!string.IsNullOrEmpty(userTypeVM.Height.ToString()))
            {
                userProfile.UserType.Height = userTypeVM.Height;
            }
            if (!string.IsNullOrEmpty(userTypeVM.Weight.ToString()))
            {
                userProfile.UserType.Weight = userTypeVM.Weight;
            }
            if (userTypeVM.BadHabits != null)
            {
                _userTypeBadHabitsService.DeleteBadHabits(userProfile.UserTypeId.Value);

                foreach (var badHabit in userTypeVM.BadHabits)
                {
                    var newBadHabit = new UserTypeBadHabits
                    {
                        BadHabitId = badHabit,
                        UserTypeId = userProfile.UserTypeId.Value
                    };

                    await Database.UserTypeBadHabits.Create(newBadHabit);
                }
            }
            if (userTypeVM.Languages != null)
            {
                _userTypeLanguagesService.DeleteLanguages(userProfile.UserTypeId.Value);

                foreach (var lang in userTypeVM.Languages)
                {
                    var newLang = new UserTypeLanguages
                    {
                        LanguageId = lang,
                        UserTypeId = userProfile.UserTypeId.Value
                    };

                    await Database.UserTypeLanguages.Create(newLang);
                }
            }

            userProfile.User.IsFirstLogin = false;

            await Database.UserProfiles.Update(userProfile);

            // Send message about 
            string message = $"{userProfile.User.UserName}, Your more personal information has been changed.";
            await _emailSenderService.SendEmailAsync(userProfile.User.Email, "Change of personal data", message);

            result.IsSuccess = true;
            result.Message = "Edit is completed";

            return result;
        }

        public async Task<ResultHelper> EditMainInformation(EditMainViewModel model)
        {
            var result = new ResultHelper();

            try
            {
                var user = await Database.Users.GetById(model.Id);

                user.UserName = model.UserName;
                user.FirstName = model.FirstName;
                user.LastName = model.LastName;
                user.GenderId = model.GenderId;
                user.DateOfBirth = model.DateOfBirth;

                // Create user profile
                var zodiacSignValue = CalculateDataHelper.CalculateZodiacSignValue(model.DateOfBirth);
                var zodiacSign = Database.ZodiacSigns.Find(x => x.Value == zodiacSignValue).FirstOrDefault();

                user.UserProfile.UserType.ZodiacSign = zodiacSign;

                //user = _mapper.Map<User>(model);
                await Database.Users.Update(user);

                // Send message about 
                string message = $"{user.UserName}, Your main personal information has been changed.";
                await _emailSenderService.SendEmailAsync(user.Email, "Change of personal data", message);

                result.IsSuccess = true;
                result.Message = "Edit info successfully";
                return result;
            }
            catch (Exception ex)
            {
                result.IsSuccess = false;
                result.Message = "Profile update failed." + ex.Message;
                return result;
            }
        }

        public async Task<ResultHelper> IsPrivateProfile(PrivateViewModel model)
        {
            var result = new ResultHelper();

            try
            {
                var userProfile = await Database.UserProfiles.GetById(model.UserProfileId);
                userProfile.IsPrivate = model.IsPrivate;
                await Database.UserProfiles.Update(userProfile);

                result.IsSuccess = true;
                result.Message = "Profile set private settings";
                return result;
            }
            catch(Exception ex)
            {
                result.IsSuccess = false;
                result.Message = "Profile set private settings." + ex.Message;
                return result;
            }
        }
    }
}
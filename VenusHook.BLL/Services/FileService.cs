﻿using AutoMapper;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Threading.Tasks;
using VenusHook.BLL.Helpers;
using VenusHook.BLL.Interfaces;
using VenusHook.BLL.ViewModels.UserProfile.UserPhoto;
using VenusHook.DAL.Interfaces;
using VenusHook.Models.Entities;

namespace VenusHook.BLL.Services
{
    public class FileService : IFileService
    {
        private IUnitOfWork Database;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly IMapper _mapper;

        public FileService(IUnitOfWork uow, IHostingEnvironment hostingEnvironment, IMapper mapper)
        {
            Database = uow;
            _hostingEnvironment = hostingEnvironment;
            _mapper = mapper;
        }

        public async Task AddUserPhoto(PhotoViewModel model)
        {
            if (model == null)
                throw new ValidationException("Photo model is missing");

            var folder = _hostingEnvironment.WebRootPath + "/Files/Images/" + model.UserProfileId;

            CreateFolder(folder);

            UploadFileHelper uploadFileHelper = new UploadFileHelper { FileSize = 5 };
            string validation = uploadFileHelper.FileUploadValidation(model.Photo);
            if (validation != null)
                throw new ValidationException(uploadFileHelper.ErrorMessage);

            var fileExt = "." + Path.GetExtension(model.Photo.FileName).Substring(1);
            var fileName = Guid.NewGuid() + fileExt;
            var fullPath = folder + "/" + fileName;

            using (var fileStream = new FileStream(fullPath, FileMode.Create))
            {
                await model.Photo.CopyToAsync(fileStream);
            }

            var url = "/Files/Images/" + model.UserProfileId + "/" + fileName;
            var photo = new Photo
            {
                Url = url,
                UserProfileId = model.UserProfileId
            };

            await Database.Photos.Create(photo);

            var userProfile = await Database.UserProfiles.GetById(model.UserProfileId);

            userProfile.AvatarId = photo.Id;
            userProfile.AvatarUrl = photo.Url;

            await Database.UserProfiles.Update(userProfile);
        }

        public async Task AddPhotoInAlbum(PhotoAlbumViewModel model)
        {
            if (model == null)
                throw new ValidationException("Photo album model is missing");

            var folder = _hostingEnvironment.WebRootPath + "/Files/Images/" + model.UserProfileId + "/Albums/" + model.Name;

            CreateFolder(folder);

            var album = new PhotoAlbum
            {
                Name = model.Name,
                Path = "/Files/Images/" + model.UserProfileId + "/Albums/" + model.Name
            };

            foreach (IFormFile photo in model.Photos)
            {
                var fileExt = "." + Path.GetExtension(photo.FileName).Substring(1);
                var fileName = Guid.NewGuid() + fileExt;
                var url = folder + "/" + fileName;

                using (var fileStream = new FileStream(url, FileMode.Create))
                {
                    await photo.CopyToAsync(fileStream);
                }

                var newPhoto = new Photo
                {
                    Url = url,
                    UserProfileId = model.UserProfileId,
                    PhotoAlbum = album
                };

                await Database.Photos.Create(newPhoto);
            }
        }

        public async Task DeletePhoto(int id)
        {
            await Database.Photos.Delete(id);
        }

        private void CreateFolder(string path)
        {
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using VenusHook.BLL.Interfaces.AdditionalInterfaces;
using VenusHook.DAL.Interfaces;

namespace VenusHook.BLL.Services.AdditionalService
{
    public class UserTypeLanguagesService : IUserTypeLanguagesService
    {
        private IUnitOfWork Database { get; set; }

        public UserTypeLanguagesService(IUnitOfWork uow)
        {
            Database = uow;
        }

        public void DeleteLanguages(int id)
        {
            var userLanguages = Database.UserTypeLanguages.Find(x => x.UserTypeId == id);

            foreach (var language in userLanguages)
            {
                Database.UserTypeLanguages.Delete(language.Id);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using VenusHook.BLL.Interfaces.AdditionalInterfaces;
using VenusHook.DAL.Interfaces;

namespace VenusHook.BLL.Services.AdditionalService
{
    public class UserTypeBadHabitsService : IUserTypeBadHabitsService
    {
        private IUnitOfWork Database { get; set; }

        public UserTypeBadHabitsService(IUnitOfWork uow)
        {
            Database = uow;
        }

        public void DeleteBadHabits(int id)
        {
            var badHabits = Database.UserTypeBadHabits.Find(x => x.UserTypeId == id);

            foreach (var badHabit in badHabits)
            {
                Database.UserTypeBadHabits.Delete(badHabit.Id);
            }
        }
    }
}

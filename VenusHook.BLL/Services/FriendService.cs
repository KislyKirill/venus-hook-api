﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VenusHook.BLL.Interfaces;
using VenusHook.BLL.ViewModels.Friendships;
using VenusHook.DAL.Interfaces;
using VenusHook.Models.Entities;
using VenusHook.Models.Enums;

namespace VenusHook.BLL.Services
{
    public class FriendService : IFriendService
    {
        private IUnitOfWork Database { get; set; }
        private readonly IMapper _mapper;

        public FriendService(IUnitOfWork uow, IMapper mapper)
        {
            Database = uow;
            _mapper = mapper;
        }

        public async Task<FriendRequest> SendRequest(FriendRequestViewModel model)
        {
            var newRequest = _mapper.Map<FriendRequest>(model);
            newRequest.FriendRequestStatus = FriendRequestStatus.Waiting;

            await Database.FriendRequests.Create(newRequest);

            return newRequest;
        }

        public async Task<IEnumerable<FriendRequest>> FindAllRequests(string userProfileId)
        {
            var userProfile = await Database.UserProfiles.GetById(userProfileId);

            return userProfile.FriendRequestReceived;
        }

        public async Task<IEnumerable<FriendshipList>> GetAllFriends(string userProfileId)
        {
            var userProfile = await Database.UserProfiles.GetById(userProfileId);
            var friendsList = userProfile.FriendshipsList;

            return friendsList;
        }

        public async Task AcceptedRequest(int id)
        {
            var findRequest = await Database.FriendRequests.GetById(id);
            findRequest.FriendRequestStatus = FriendRequestStatus.Accept;

            await Database.FriendRequests.Update(findRequest);
        }

        public async Task RejectedRequest(int id)
        {
            await Database.FriendRequests.Delete(id);
        }

        public async Task RemoveFriend(string userProfileId, string friendId)
        {
            var friendship = Database.FriendshipLists.Find(x => x.UserProfileId == userProfileId && x.FriendId == friendId).First();

            if (friendship == null)
            {
                throw new ValidationException("Friend not found");
            }

            var waitingRequest = Database.FriendRequests.Find(x => x.SenderId == friendId && x.ReceiverId == userProfileId).First();
            waitingRequest.FriendRequestStatus = FriendRequestStatus.Waiting;

            await Database.FriendRequests.Update(waitingRequest);
            await Database.FriendshipLists.Delete(friendship);
        }
    }
}

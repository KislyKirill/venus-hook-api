﻿using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using VenusHook.BLL.Helpers;
using VenusHook.BLL.Interfaces;
using VenusHook.Models.Settings;
using VenusHook.DAL.Interfaces;
using VenusHook.Models.Entities;
using System.Linq;
using VenusHook.BLL.ViewModels.User;
using VenusHook.BLL.ViewModels.Settings;

namespace VenusHook.BLL.Services
{
    public class UserService : IUserService
    {
        private readonly IUnitOfWork Database;

        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        private readonly IMapper _mapper;
        private readonly IEmailSenderService _emailSenderService;
        private readonly ApplicationSettings _appSetings;

        public UserService(IUnitOfWork uow,
            UserManager<User> userManager,
            SignInManager<User> signInManager,
            IMapper mapper,
            IEmailSenderService emailSenderService,
            IOptions<ApplicationSettings> appSettings)
        {
            Database = uow;
            _userManager = userManager;
            _signInManager = signInManager;
            _mapper = mapper;
            _emailSenderService = emailSenderService;
            _appSetings = appSettings.Value;
        }

        public async Task<ResultHelper> Register(RegisterViewModel model, string host)
        {
            User user = _mapper.Map<User>(model);
            user.IsFirstLogin = true;

            var result = await _userManager.CreateAsync(user, model.Password);
            var resultHelper = new ResultHelper() { IsSuccess = result.Succeeded };

            if (resultHelper.IsSuccess)
            {
                // Create user profile
                var zodiacSignValue = CalculateDataHelper.CalculateZodiacSignValue(model.DateOfBirth);
                var zodiacSign = Database.ZodiacSigns.Find(x => x.Value == zodiacSignValue).FirstOrDefault();

                var userProfile = new UserProfile
                {
                    Id = user.Id,
                    UserType = new UserType
                    {
                        ZodiacSign = zodiacSign
                    }
                };
                await Database.UserProfiles.Create(userProfile);

                // Send confirmation email
                var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
                var encode = HttpUtility.UrlEncode(code);
                var callbackUrl = new StringBuilder();
                callbackUrl.Append(host)
                    .Append("/api/Users/ConfirmEmail")
                    .Append("?userId=")
                    .Append(user.Id)
                    .Append("&code=")
                    .Append(encode);

                string message = $"{user.UserName}, you are welcomed by the administration of the site Venus Hook. Confirm your registration on the website by clicking on the link: <a href='{callbackUrl}'>click to confirm</a>";
                await _emailSenderService.SendEmailAsync(user.Email, "Account verification", message);
            }
            resultHelper.Property = result;

            return resultHelper;
        }

        public async Task<ResultHelper> ConfirmationEmail(string userId = "", string code = "")
        {
            var resultHelper = new ResultHelper();

            if (string.IsNullOrWhiteSpace(userId) || string.IsNullOrWhiteSpace(code))
            {
                resultHelper.IsSuccess = false;
                resultHelper.Property = "User id not set or link incorrect.";

                return resultHelper;
            }

            var user = await _userManager.FindByIdAsync(userId);
            if (user == null)
            {
                resultHelper.IsSuccess = false;
                resultHelper.Property = "User not found";

                return resultHelper;
            }

            var result = await _userManager.ConfirmEmailAsync(user, code);
            if (result.Succeeded)
            {
                await _signInManager.SignInAsync(user, false);
                resultHelper.IsSuccess = true;
            }
            else
            {
                resultHelper.Property = result.Errors;
            }

            return resultHelper;
        }

        public async Task<ResultHelper> Login(LoginViewModel model)
        {
            var result = new ResultHelper();

            var user = await _userManager.FindByNameAsync(model.UserName);
            if (user != null && await _userManager.CheckPasswordAsync(user, model.Password))
            {
                if (!await _userManager.IsEmailConfirmedAsync(user))
                {
                    result.Message = "You have not confirmed your email.";
                    result.IsSuccess = false;
                }
                else
                {
                    var tokenDescriptor = new SecurityTokenDescriptor
                    {
                        Subject = new ClaimsIdentity(new Claim[]
                           {
                                new Claim("UserId", user.Id.ToString())
                           }),
                        Expires = DateTime.UtcNow.AddDays(15),
                        SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_appSetings.JwtSecret)), SecurityAlgorithms.HmacSha256Signature)
                    };
                    var tokenHadler = new JwtSecurityTokenHandler();
                    var securityToken = tokenHadler.CreateToken(tokenDescriptor);
                    var token = tokenHadler.WriteToken(securityToken);

                    result.IsSuccess = true;
                    result.Property = token;
                }
            }
            else
            {
                result.Message = "Username or password is incorrect.";
            }

            return result;
        }

        public async Task<ResultHelper> ChangePassword(ChangePasswordViewModel model)
        {
            var result = new ResultHelper();

            var user = await _userManager.FindByIdAsync(model.Id);
            if (user != null)
            {
                IdentityResult identityResult = await _userManager.ChangePasswordAsync(user, model.OldPassword, model.NewPassword);
                result.IsSuccess = identityResult.Succeeded;

                if (result.IsSuccess)
                {
                    // Send message about 
                    string message = $"{user.UserName}, Your password has been changed.";
                    await _emailSenderService.SendEmailAsync(user.Email, "Change password", message);

                    result.Message = "Change password successful";
                    return result;
                }
                else
                {
                    foreach (var error in identityResult.Errors)
                    {
                        result.Message += error.Description + "\n";
                    }
                }
            }
            else
            {
                result.Message = "User not found";
            }

            return result;
        }
    }
}

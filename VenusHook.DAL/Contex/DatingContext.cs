﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using VenusHook.DAL.Contex.Configurations;
using VenusHook.Models.Entities;
using VenusHook.Models.Entities.Type;

namespace VenusHook.DAL.Contex
{
    public class DatingContext : IdentityDbContext<User>
    {
        public DbSet<UserProfile> UserProfiles { get; set; }
        public DbSet<UserType> UserTypes { get; set; }
        public DbSet<Statistics> Statistics { get; set; }
        public DbSet<Photo> Photos { get; set; }
        public DbSet<PhotoAlbum> PhotoAlbums { get; set; }
        public DbSet<Message> Messages { get; set; }
        public DbSet<FriendshipList> FriendshipsList { get; set; }
        public DbSet<FriendRequest> FriendRequest { get; set; }
        public DbSet<BlackList> BlackList { get; set; }
        public DbSet<BadHabit> BadHabits { get; set; }
        public DbSet<Education> Education { get; set; }
        public DbSet<FamilyStatus> FamilyStatus { get; set; }
        public DbSet<FinancialSituation> FinancialSituation { get; set; }
        public DbSet<Gender> Gender { get; set; }
        public DbSet<Language> Languages { get; set; }
        public DbSet<Nationality> Nationalities { get; set; }
        public DbSet<ZodiacSign> ZodiacSigns { get; set; }
        public DbSet<UserTypeBadHabits> UserTypeBadHabits { get; set; }
        public DbSet<UserTypeLanguages> UserTypeLanguages { get; set; }


        public DatingContext(DbContextOptions<DatingContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ApplyConfiguration(new UserProfileConfiguration());
            builder.ApplyConfiguration(new FriendRequestConfiguration());

            base.OnModelCreating(builder);
        }
    }
}

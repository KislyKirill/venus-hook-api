﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using VenusHook.Models.Entities;

namespace VenusHook.DAL.Contex.Configurations
{
    internal class UserProfileConfiguration : IEntityTypeConfiguration<UserProfile>
    {
        public void Configure(EntityTypeBuilder<UserProfile> builder)
        {
            builder
                .HasMany(a => a.MessagesReceived)
                .WithOne(b => b.Receiver)
                .HasForeignKey(c => c.ReceiverId)
                .OnDelete(DeleteBehavior.Restrict);

            builder
                .HasMany(a => a.MessagesSent)
                .WithOne(b => b.Sender)
                .HasForeignKey(c => c.SenderId)
                .OnDelete(DeleteBehavior.Restrict);

            builder
                .HasMany(a => a.FriendshipsList)
                .WithOne(b => b.Friend)
                .HasForeignKey(c => c.FriendId);

            builder
                .HasMany(a => a.BlackList)
                .WithOne(b => b.BlockUser)
                .HasForeignKey(c => c.UserProfileId);
        }
    }
}

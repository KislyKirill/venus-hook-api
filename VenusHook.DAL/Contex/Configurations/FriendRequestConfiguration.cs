﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using VenusHook.Models.Entities;

namespace VenusHook.DAL.Contex.Configurations
{
    internal class FriendRequestConfiguration : IEntityTypeConfiguration<FriendRequest>
    {

        public void Configure(EntityTypeBuilder<FriendRequest> builder)
        {
            builder
                .HasOne(a => a.Sender)
                .WithMany(b => b.FriendRequestSent)
                .HasForeignKey(c => c.SenderId);

            builder
                .HasOne(a => a.Receiver)
                .WithMany(b => b.FriendRequestReceived)
                .HasForeignKey(c => c.ReceiverId);
        }
    }
}

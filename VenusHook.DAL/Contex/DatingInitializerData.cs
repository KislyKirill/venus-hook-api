﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Linq;
using System.Threading.Tasks;
using VenusHook.Models.Entities;
using VenusHook.Models.Entities.Type;

namespace VenusHook.DAL.Contex
{
    public static class DatingInitializerData
    {
        public static async Task InitialData(DatingContext context, UserManager<User> userManager)
        {
            // First initial data
            if (!context.Languages.Any())
            {
                context.Languages.AddRange(
                    new Language { Value = "Russian" },
                    new Language { Value = "Belarusian" },
                    new Language { Value = "Ukrainian" },
                    new Language { Value = "Kazakh" },
                    new Language { Value = "English" },
                    new Language { Value = "Spanish" },
                    new Language { Value = "Chinese" },
                    new Language { Value = "Portuguese" },
                    new Language { Value = "French" },
                    new Language { Value = "Italian" });
            }
            if (!context.BadHabits.Any())
            {
                context.BadHabits.AddRange(
                    new BadHabit { Value = "Smoking" },
                    new BadHabit { Value = "Drink" },
                    new BadHabit { Value = "Shopping" },
                    new BadHabit { Value = "Nail Biting" },
                    new BadHabit { Value = "Stress Eating" });
            }
            if (!context.Education.Any())
            {
                context.Education.AddRange(
                    new Education { Value = "High school" },
                    new Education { Value = "College" },
                    new Education { Value = "University" },
                    new Education { Value = "Master`s degree" },
                    new Education { Value = "Postgraduate study" });
            }
            if (!context.FamilyStatus.Any())
            {
                context.FamilyStatus.AddRange(
                    new FamilyStatus { Value = "Not married" },
                    new FamilyStatus { Value = "Dating" },
                    new FamilyStatus { Value = "Engaged" },
                    new FamilyStatus { Value = "Married" },
                    new FamilyStatus { Value = "In a civil marriage" },
                    new FamilyStatus { Value = "In love" },
                    new FamilyStatus { Value = "Everything is difficult" },
                    new FamilyStatus { Value = "In active search" });
            }
            if (!context.FinancialSituation.Any())
            {
                context.FinancialSituation.AddRange(
                    new FinancialSituation { Value = "Poor" },
                    new FinancialSituation { Value = "Middle" },
                    new FinancialSituation { Value = "Rich" });
            }
            if (!context.Gender.Any())
            {
                context.Gender.AddRange(
                    new Gender { Value = "Male" },
                    new Gender { Value = "Female" });
            }
            if (!context.ZodiacSigns.Any())
            {
                context.ZodiacSigns.AddRange(
                    new ZodiacSign { Value = "Aquarius" },
                    new ZodiacSign { Value = "Pisces" },
                    new ZodiacSign { Value = "Aries" },
                    new ZodiacSign { Value = "Taurus" },
                    new ZodiacSign { Value = "Gemini" },
                    new ZodiacSign { Value = "Cancer" },
                    new ZodiacSign { Value = "Leo" },
                    new ZodiacSign { Value = "Virgo" },
                    new ZodiacSign { Value = "Libra" },
                    new ZodiacSign { Value = "Scorpio" },
                    new ZodiacSign { Value = "Sagittarius" },
                    new ZodiacSign { Value = "Capricorn" });
            }
            if (!context.Nationalities.Any())
            {
                context.Nationalities.AddRange(
                    new Nationality { Value = "Russian" },
                    new Nationality { Value = "Belarusian" },
                    new Nationality { Value = "Ukrainian" },
                    new Nationality { Value = "Kazakh" },
                    new Nationality { Value = "English" },
                    new Nationality { Value = "Chinese" },
                    new Nationality { Value = "Spanish" },
                    new Nationality { Value = "Portuguese" },
                    new Nationality { Value = "French" },
                    new Nationality { Value = "Italian" });
            }
            await context.SaveChangesAsync();



            if (!context.Users.Any())
            {
                string password = "123Asd#";
                DateTime dateOfBirth = new DateTime(1996, 07, 04);

                for (int count = 1; count <= 3; count++)
                {
                    User user = new User { Email = $"tUser{count}@gmail.com", UserName = $"User{count}", FirstName = "Ivan", LastName = "Ivanonich", EmailConfirmed = true, GenderId = 1, DateOfBirth = dateOfBirth };
                    await userManager.CreateAsync(user, password);
                    UserProfile userProfile = new UserProfile
                    {
                        Id = user.Id,
                        UserType = new UserType
                        {
                            Height = 176,
                            Weight = 83,
                            Interests = "My interests are programming ",
                            ZodiacSignId = 2
                        }
                    };
                    context.Add(userProfile);
                }

                for (int count = 4; count <= 6; count++)
                {
                    User user = new User { Email = $"tUser{count}@gmail.com", UserName = $"User{count}", FirstName = "Alex", LastName = "Letv", EmailConfirmed = true, GenderId = 1, DateOfBirth = dateOfBirth };
                    await userManager.CreateAsync(user, password);
                    UserProfile userProfile = new UserProfile
                    {
                        Id = user.Id,
                        UserType = new UserType
                        {
                            Height = 189,
                            Weight = 71,
                            Interests = "I am very fond of swimming, I like to watch movies, I recommend to watch the movie start",
                            ZodiacSignId = 7
                        }
                    };
                    context.Add(userProfile);
                }
            
                for (int count = 7; count <= 9; count++)
                {
                    User user = new User { Email = $"tUser{count}@gmail.com", UserName = $"User{count}", FirstName = "Petr", LastName = "Zuev", EmailConfirmed = true, GenderId = 1, DateOfBirth = dateOfBirth };
                    await userManager.CreateAsync(user, password);
                    UserProfile userProfile = new UserProfile
                    {
                        Id = user.Id,
                        UserType = new UserType
                        {
                            Height = 193,
                            Weight = 96,
                            Interests = "I am very fond of swimming, I like to watch movies, I recommend to watch the movie start",
                            ZodiacSignId = 7
                        }
                    };
                    context.Add(userProfile);
                }
            }
            await context.SaveChangesAsync();
        }
    }
}

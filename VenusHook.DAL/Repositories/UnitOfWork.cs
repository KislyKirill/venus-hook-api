﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using VenusHook.DAL.Contex;
using VenusHook.DAL.Interfaces;
using VenusHook.Models.Entities;
using VenusHook.Models.Entities.Type;

namespace VenusHook.DAL.Repositories
{
    public class UnitOfWork : IUnitOfWork, IDisposable
    {
        private readonly DatingContext _context;
        private IGenericRepository<PhotoAlbum> _photoAlbums;
        private IGenericRepository<BlackList> _blackLists;
        private IGenericRepository<FriendRequest> _friendRequests;
        private IGenericRepository<FriendshipList> _friendshipLists;
        private IGenericRepository<Message> _messages;
        private IGenericRepository<Photo> _photos;
        private IGenericRepository<Statistics> _statistics;
        private IGenericRepository<UserType> _userTypes;
        private IGenericRepository<BadHabit> _badHabits;
        private IGenericRepository<Education> _education;
        private IGenericRepository<FamilyStatus> _familyStatus;
        private IGenericRepository<FinancialSituation> _financialSituations;
        private IGenericRepository<Gender> _gender;
        private IGenericRepository<Language> _languages;
        private IGenericRepository<Nationality> _nationalities;
        private IGenericRepository<ZodiacSign> _zodiacSigns;
        private IGenericRepository<User> _users;
        private IGenericRepository<UserProfile> _userProfiles;
        private IGenericRepository<UserTypeBadHabits> _userTypeBadHabits;
        private IGenericRepository<UserTypeLanguages> _userTypeLanguages;

        public UnitOfWork(DatingContext context)
        {
            _context = context;
        }

        public IGenericRepository<PhotoAlbum> PhotoAlbums => _photoAlbums ?? (_photoAlbums = new GenericRepository<PhotoAlbum>(_context));
        public IGenericRepository<BlackList> BlackLists => _blackLists ?? (_blackLists = new GenericRepository<BlackList>(_context));
        public IGenericRepository<FriendRequest> FriendRequests => _friendRequests ?? (_friendRequests = new GenericRepository<FriendRequest>(_context));
        public IGenericRepository<FriendshipList> FriendshipLists => _friendshipLists ?? (_friendshipLists = new GenericRepository<FriendshipList>(_context));
        public IGenericRepository<Message> Messages => _messages ?? (_messages = new GenericRepository<Message>(_context));
        public IGenericRepository<Photo> Photos => _photos ?? (_photos = new GenericRepository<Photo>(_context));
        public IGenericRepository<Statistics> Statistics => _statistics ?? (_statistics = new GenericRepository<Statistics>(_context));
        public IGenericRepository<UserType> UserTypes => _userTypes ?? (_userTypes = new GenericRepository<UserType>(_context));
        public IGenericRepository<BadHabit> BadHabits => _badHabits ?? (_badHabits = new GenericRepository<BadHabit>(_context));
        public IGenericRepository<Education> Education => _education ?? (_education = new GenericRepository<Education>(_context));
        public IGenericRepository<FamilyStatus> FamilyStatus => _familyStatus ?? (_familyStatus = new GenericRepository<FamilyStatus>(_context));
        public IGenericRepository<FinancialSituation> FinancialSituations => _financialSituations ?? (_financialSituations = new GenericRepository<FinancialSituation>(_context));
        public IGenericRepository<Gender> Gender => _gender ?? (_gender = new GenericRepository<Gender>(_context));
        public IGenericRepository<Language> Languages => _languages ?? (_languages = new GenericRepository<Language>(_context));
        public IGenericRepository<Nationality> Nationalities => _nationalities ?? (_nationalities = new GenericRepository<Nationality>(_context));
        public IGenericRepository<ZodiacSign> ZodiacSigns => _zodiacSigns ?? (_zodiacSigns = new GenericRepository<ZodiacSign>(_context));
        public IGenericRepository<User> Users => _users ?? (_users = new GenericRepository<User>(_context));
        public IGenericRepository<UserProfile> UserProfiles => _userProfiles ?? (_userProfiles = new GenericRepository<UserProfile>(_context));
        public IGenericRepository<UserTypeBadHabits> UserTypeBadHabits => _userTypeBadHabits ?? (_userTypeBadHabits = new GenericRepository<UserTypeBadHabits>(_context));
        public IGenericRepository<UserTypeLanguages> UserTypeLanguages => _userTypeLanguages ?? (_userTypeLanguages = new GenericRepository<UserTypeLanguages>(_context));

        public async Task SaveAsync()
        {
            await _context.SaveChangesAsync();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            System.GC.SuppressFinalize(this);
        }
    }
}
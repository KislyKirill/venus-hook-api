﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace VenusHook.DAL.Migrations
{
    public partial class Friendrequeststatus : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_FriendRequest_FriendRequestStatus_FriendRequestStatusId",
                table: "FriendRequest");

            migrationBuilder.DropTable(
                name: "FriendRequestStatus");

            migrationBuilder.DropIndex(
                name: "IX_FriendRequest_FriendRequestStatusId",
                table: "FriendRequest");

            migrationBuilder.DropColumn(
                name: "FriendRequestStatusId",
                table: "FriendRequest");

            migrationBuilder.AddColumn<int>(
                name: "FriendRequestStatus",
                table: "FriendRequest",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FriendRequestStatus",
                table: "FriendRequest");

            migrationBuilder.AddColumn<int>(
                name: "FriendRequestStatusId",
                table: "FriendRequest",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "FriendRequestStatus",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Value = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FriendRequestStatus", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_FriendRequest_FriendRequestStatusId",
                table: "FriendRequest",
                column: "FriendRequestStatusId");

            migrationBuilder.AddForeignKey(
                name: "FK_FriendRequest_FriendRequestStatus_FriendRequestStatusId",
                table: "FriendRequest",
                column: "FriendRequestStatusId",
                principalTable: "FriendRequestStatus",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}

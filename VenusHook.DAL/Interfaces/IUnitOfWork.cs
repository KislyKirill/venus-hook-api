﻿using System.Threading.Tasks;
using VenusHook.Models.Entities;
using VenusHook.Models.Entities.Type;

namespace VenusHook.DAL.Interfaces
{
    public interface IUnitOfWork
    {
        IGenericRepository<PhotoAlbum> PhotoAlbums { get; }
        IGenericRepository<BlackList> BlackLists { get; }
        IGenericRepository<FriendRequest> FriendRequests { get; }
        IGenericRepository<FriendshipList> FriendshipLists { get; }
        IGenericRepository<Message> Messages { get; }
        IGenericRepository<Photo> Photos { get; }
        IGenericRepository<Statistics> Statistics { get; }
        IGenericRepository<UserType> UserTypes { get; }
        IGenericRepository<BadHabit> BadHabits { get; }
        IGenericRepository<Education> Education { get; }
        IGenericRepository<FamilyStatus> FamilyStatus { get; }
        IGenericRepository<FinancialSituation> FinancialSituations { get; }
        IGenericRepository<Gender> Gender { get; }
        IGenericRepository<Language> Languages { get; }
        IGenericRepository<Nationality> Nationalities { get; }
        IGenericRepository<ZodiacSign> ZodiacSigns { get; }
        IGenericRepository<User> Users { get; }
        IGenericRepository<UserProfile> UserProfiles { get; }
        IGenericRepository<UserTypeBadHabits> UserTypeBadHabits { get; }
        IGenericRepository<UserTypeLanguages> UserTypeLanguages { get; }

        Task SaveAsync();
    }
}

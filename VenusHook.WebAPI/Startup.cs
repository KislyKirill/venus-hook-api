﻿using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Text;
using VenusHook.BLL.Helpers;
using VenusHook.BLL.Interfaces;
using VenusHook.BLL.Services;
using VenusHook.Models.Settings;
using VenusHook.DAL.Contex;
using VenusHook.DAL.Interfaces;
using VenusHook.DAL.Repositories;
using VenusHook.Models.Entities;
using VenusHook.BLL.Interfaces.AdditionalInterfaces;
using VenusHook.BLL.Services.AdditionalService;

namespace VenusHook.WebAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<EmailSettings>(Configuration.GetSection("EmailSettings"));
            services.Configure<ApplicationSettings>(Configuration.GetSection("ApplicationSettings"));

            // Connection context
            string connection = Configuration.GetConnectionString("DatingConnection");
            services.AddEntityFrameworkProxies();
            services.AddDbContext<DatingContext>(options =>
            {
                options.UseMySql(connection);
                options.UseLazyLoadingProxies(true);
            });

            // Auto Mapper Configurations
            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new AutoMapperProfile());
            });
            IMapper mapper = mappingConfig.CreateMapper();
            services.AddSingleton(mapper);

            // Identity
            services.AddIdentity<User, IdentityRole>()
                .AddEntityFrameworkStores<DatingContext>()
                .AddDefaultTokenProviders();
            services.Configure<IdentityOptions>(options =>
            {
                options.Password.RequireDigit = true;
                options.Password.RequireNonAlphanumeric = true;
                options.Password.RequireLowercase = true;
                options.Password.RequireUppercase = true;
                options.Password.RequiredLength = 6;
            });

            // Add our service
            services.AddSingleton<IEmailSenderService, EmailSenderService>();
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IUserProfileService, UserProfileService>();
            services.AddScoped<IUserTypeService, UserTypeService>();
            services.AddScoped<IFileService, FileService>();
            services.AddScoped<ISearchService, SearchService>();
            services.AddScoped<IFriendService, FriendService>();
            services.AddTransient<IUserTypeLanguagesService, UserTypeLanguagesService>();
            services.AddTransient<IUserTypeBadHabitsService, UserTypeBadHabitsService>();


            // JWT authentication
            var key = Encoding.UTF8.GetBytes(Configuration["ApplicationSettings:JwtSecret"].ToString());

            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(x =>
            {
                x.RequireHttpsMetadata = false;
                x.SaveToken = false;
                x.TokenValidationParameters = new Microsoft.IdentityModel.Tokens.TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    ClockSkew = TimeSpan.Zero
                };
            });

            services.AddCors();
            services.AddMvc()
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_1)
                .AddJsonOptions(x => x.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore);
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseCors(builder =>
                builder.WithOrigins(Configuration["ApplicationSettings:ClientUrl"].ToString())
                    .AllowAnyHeader()
                    .AllowAnyMethod());

            app.UseAuthentication();

            app.UseStaticFiles();

            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}

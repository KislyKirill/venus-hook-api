﻿using System;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using VenusHook.BLL.Helpers;
using VenusHook.BLL.Interfaces;
using VenusHook.BLL.ViewModels.Settings;
using VenusHook.BLL.ViewModels.UserProfile;
using VenusHook.BLL.ViewModels.UserProfile.EditProfile;
using VenusHook.BLL.ViewModels.UserProfile.UserPhoto;

namespace VenusHook.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserProfilesController : ControllerBase
    {
        private readonly IUserProfileService _userProfileService;
        private readonly IUserTypeService _userTypeService;
        private readonly IUserService _userService;
        private readonly IFileService _fileService;
        private readonly IMapper _mapper;

        public UserProfilesController(IUserProfileService userProfileService, IMapper mapper, IUserTypeService userTypeService, IUserService userService, IFileService fileService)
        {
            _userProfileService = userProfileService;
            _userTypeService = userTypeService;
            _userService = userService;
            _fileService = fileService;
            _mapper = mapper;
        }

        [HttpGet]
        [Authorize]
        // GET: /api/UserProfiles
        public async Task<object> GetUserProfiles()
        {
            try
            {
                string userId = User.Claims.First(c => c.Type == "UserId").Value ?? null;
                var userProfile = await _userProfileService.GetUserProfile(userId);

                var userProfileViewModel = _mapper.Map<UserProfileViewModel>(userProfile);

                return Ok(userProfileViewModel);
            }
            catch (Exception ex)
            {
                return BadRequest(new { ex.Message });
            }
        }

        [HttpGet]
        [Route("AdditionalInfo")]
        // GET: /api/UserProfiles/additionalInfo
        public IActionResult GetAdditionalInfo()
        {
            var userTypes = _userTypeService.GetAllAdditionalInfo();
            return Ok(userTypes);
        }

        [HttpPost]
        [Authorize]
        [Route("CompletedRegister")]
        // POST: /api/UserProfiles/CompletedRegister
        public async Task<IActionResult> CompletedRegister(MoreRegisterViewModel model)
        {
            string userId = User.Claims.First(c => c.Type == "UserId").Value ?? null;
            var userProfile = await _userProfileService.GetUserProfile(userId);

            var result = await _userProfileService.EditMoreInformation(model, userProfile);

            return Ok(result);
        }

        [HttpPost]
        [Authorize]
        [Route("UploadImages")]
        // POST: /api/UserProfiles/UploadImages
        public async Task<IActionResult> UploadImages()
        {
            try
            {
                var userId = User.Claims.First(c => c.Type == "UserId").Value;
                var file = HttpContext.Request.Form.Files[0];

                var photoViewModel = new PhotoViewModel
                {
                    UserProfileId = userId,
                    Photo = file
                };

                await _fileService.AddUserPhoto(photoViewModel);

                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(new { ex.Message });
            }
        }

        [HttpPut]
        [Authorize]
        [Route("EditProfile")]
        // PUT: /api/UserProfiles/EditProfile
        public async Task<IActionResult> PutUserProfiles(EditMainViewModel model)
        {
            var result = new ResultHelper();
            try
            {
                var userId = User.Claims.First(c => c.Type == "UserId").Value;
                model.Id = userId;

                result = await _userProfileService.EditMainInformation(model);

                return Ok(result);
            }
            catch (Exception)
            {
                return BadRequest(new { result.Message });
            }
        }

        [HttpPut]
        [Authorize]
        [Route("EditMoreProfile")]
        // PUT: /api/UserProfiles/EditMoreProfile
        public async Task<IActionResult> PutUserProfiles(MoreRegisterViewModel model)
        {
            var result = new ResultHelper();

            try
            {

                string userId = User.Claims.First(c => c.Type == "UserId").Value ?? null;
                var userProfile = await _userProfileService.GetUserProfile(userId);

                result = await _userProfileService.EditMoreInformation(model, userProfile);

                return Ok(result);
            }
            catch (Exception)
            {
                return BadRequest(new { result.Message });
            }
        }

        [HttpPut]
        [Authorize]
        [Route("PrivateSettings")]
        // PUT: /api/UserProfiles/PrivateSettings
        public async Task<IActionResult> PutUserProfiles(PrivateViewModel model)
        {
            var result = new ResultHelper();

            try
            {
                string userId = User.Claims.First(c => c.Type == "UserId").Value ?? null;
                model.UserProfileId = userId;

                result = await _userProfileService.IsPrivateProfile(model);

                return Ok(result);
            }
            catch (Exception)
            {
                return BadRequest(new { result.Message });
            }
        }

        [HttpPut]
        [Authorize]
        [Route("ChangePassword")]
        // PUT: /api/UserProfiles/ChangePassword
        public async Task<IActionResult> PutUserProfiles(ChangePasswordViewModel model)
        {
            var result = new ResultHelper();

            try
            {
                string userId = User.Claims.First(c => c.Type == "UserId").Value ?? null;
                model.Id = userId;

                result = await _userService.ChangePassword(model);
                if (result.IsSuccess)
                    return Ok(result);
                else
                    return BadRequest(new { result.Message });
            }
            catch (Exception)
            {
                return BadRequest(new { result.Message });
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using VenusHook.BLL.Interfaces;
using VenusHook.BLL.ViewModels.Friendships;
using VenusHook.BLL.ViewModels.UserProfile;

namespace VenusHook.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class FriendsController : ControllerBase
    {
        private readonly IFriendService _friendService;
        private readonly IUserProfileService _userProfileService;
        private readonly IMapper _mapper;

        public FriendsController(IFriendService friendService, IMapper mapper, IUserProfileService userProfileService)
        {
            _friendService = friendService;
            _userProfileService = userProfileService;
            _mapper = mapper;
        }

        [HttpGet]
        // GET: /api/Friends
        public async Task<IActionResult> GetFriends()
        {
            var currentUserId = User.Claims.First(c => c.Type == "UserId").Value ?? null;
            var friends = await _friendService.GetAllFriends(currentUserId);

            if (friends != null)
                return Ok(_mapper.Map<FriendshipListViewModel>(friends));
            else
                return NoContent();
        }


        [HttpGet]
        [Route("FriendDetails/{id}")]
        //GET: api/Friends/FriendDetails/{id}
        public async Task<IActionResult> GetFriends(string id)
        {
            var friend = await _userProfileService.GetUserProfile(id);
            return Ok(_mapper.Map<UserProfileViewModel>(friend));
        }

        [HttpPost]
        [Route("SendRequest/{userId}")]
        // POST: /api/Friends/SendRequest/{id}
        public async Task<IActionResult> PostFriends(string userProfileId)
        {
            var currentUserId = User.Claims.First(c => c.Type == "UserId").Value ?? null;

            var newRequest = new FriendRequestViewModel
            {
                SenderId = currentUserId,
                ReceiverId = userProfileId
            };

            var sendRequest = await _friendService.SendRequest(newRequest);

            if (sendRequest == null)
                return BadRequest();
            else
                return Ok(sendRequest);
        }

        [HttpGet]
        [Route("Requests")]
        // GET: /api/Friends/Requests
        public async Task<IEnumerable<FriendRequestViewModel>> GetFriendRequests()
        {
            var currentUserId = User.Claims.First(c => c.Type == "UserId").Value ?? null;
            var friendRequests = await _friendService.FindAllRequests(currentUserId);

            if (friendRequests != null)
                return _mapper.Map<IEnumerable<FriendRequestViewModel>>(friendRequests);
            else
                return null;
        }

        [HttpGet]
        [Route("DeleteFriend/{friendId}")]
        // GET: /api/Friends/DeleteFriend/{friendId}
        public IActionResult DeleteFriend(string friendId)
        {
            try
            {
                var currentUserId = User.Claims.First(c => c.Type == "UserId").Value ?? null;
                _friendService.RemoveFriend(currentUserId, friendId);

                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(new { ex.Message });
            }
        }

        [HttpGet]
        [Route("AcceptRequest/{id}")]
        //GET: api/Friends/AcceptRequest/{id}
        public IActionResult AcceptRequest(int id)
        {
            _friendService.AcceptedRequest(id);
            return Ok();
        }

        [HttpGet]
        [Route("RejectRequest/{id}")]
        //GET: api/Friends/RejectRequest/{id}
        public IActionResult RejectRequest(int id)
        {
            _friendService.RejectedRequest(id);
            return Ok();
        }
    }
}
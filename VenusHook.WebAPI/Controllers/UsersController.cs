﻿using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using VenusHook.BLL.Helpers;
using VenusHook.BLL.Interfaces;
using VenusHook.BLL.ViewModels.User;
using VenusHook.Models.Entities;
using VenusHook.Models.Settings;

namespace VenusHook.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IUserService _userService;
        private readonly IEmailSenderService _emailSenderSerivce;
        private readonly IMapper _mapper;
        private readonly ApplicationSettings _appSetings;

        public UsersController(IUserService userService,
            IEmailSenderService emailSenderService,
            IMapper mapper,
            IOptions<ApplicationSettings> appSettings)
        {
            _userService = userService;
            _emailSenderSerivce = emailSenderService;
            _mapper = mapper;
            _appSetings = appSettings.Value;
        }

        [HttpPost]
        [Route("Register")]
        // POST /api/Users/Register
        public async Task<IActionResult> Register(RegisterViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return Ok(ModelState);
            }

            //var user = _mapper.Map<User>(model);

            var host = new StringBuilder();
            host.Append(HttpContext.Request.Scheme)
                .Append("://")
                .Append(HttpContext.Request.Host);

            ResultHelper result = await _userService.Register(model, host.ToString());

            //if (!result.IsSuccess)
            //    return BadRequest(result.Property);

            return Ok(result.Property);
        }

        [HttpGet]
        [Route("ConfirmEmail")]
        // GET: /api/Users/ConfirmEmail
        public async Task<IActionResult> ConfirmEmail([FromQuery] string userId = "", [FromQuery] string code = "")
        {
            ResultHelper result = await _userService.ConfirmationEmail(userId, code);
            if (result.IsSuccess)
            {
                return Redirect(_appSetings.ClientUrl + "/user/login");
            }
            else
            {
                return BadRequest(result.Property);
            }
        }

        [HttpPost]
        [Route("Login")]
        // POST /api/Users/Login
        public async Task<IActionResult> Login(LoginViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            //LoginDTO loginDto = _mapper.Map<LoginDTO>(model);

            ResultHelper result = await _userService.Login(model);
            if (!result.IsSuccess)
            {
                return BadRequest(new { result.Message });
            }

            var token = result.Property;
            return Ok(new { token });
        }
    }
}
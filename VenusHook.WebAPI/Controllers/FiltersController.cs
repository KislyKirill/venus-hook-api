﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VenusHook.BLL.Interfaces;
using VenusHook.BLL.ViewModels.UserProfile;
using VenusHook.Models.Entities;

namespace VenusHook.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FiltersController : ControllerBase
    {
        private readonly ISearchService _searchService;
        private readonly IMapper _mapper;

        public FiltersController(ISearchService searchService, IMapper mapper)
        {
            _searchService = searchService;
            _mapper = mapper;
        }

        [HttpGet]
        [Authorize]
        // GET: /api/Filters
        public IEnumerable<UserProfileViewModel> GetUserProfiles()
        {
            var currentUserId = User.Claims.First(c => c.Type == "UserId").Value ?? null;
            var userProfiles = _searchService.GetAllUserProfiles(currentUserId);

            var userProfilesViewModel = _mapper.Map<IEnumerable<UserProfileViewModel>>(userProfiles);

            return userProfilesViewModel;
        }

    }
}